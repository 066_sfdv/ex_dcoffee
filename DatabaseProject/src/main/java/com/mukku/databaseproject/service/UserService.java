/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.databaseproject.service;

import com.mukku.databaseproject.dao.UserDao;
import com.mukku.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author acer
 */
public class UserService {

    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public List<User> getUser() {
        UserDao userDao = new UserDao();
//        return userDao.getAll("1=1", "user_name ASC");
        return userDao.getAll("user_name asc");
    }
    
    public User updateUser(User user){
        UserDao userDao = new UserDao();
        return userDao.update(user);
    }
    
    public User newUser(User user){
        UserDao userDao = new UserDao();
        return userDao.save(user);
    }
    
    public int deleteUser(User user){
        UserDao userDao = new UserDao();
        return userDao.delete(user);
    }
}
