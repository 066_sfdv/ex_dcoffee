/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mukku.databaseproject;

import com.mukku.databaseproject.dao.UserDao;
import com.mukku.databaseproject.helper.DatabaseHelper;
import com.mukku.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
//        System.out.println(userDao.getAll());
        //test getAll()
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }
//        //test get()
//        User user1 = userDao.get(2);
//        System.out.println(user1);
//        //test save()
//        User newUser = new User("user4", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        //test update()
//        User updateUser = userDao.get(4);
//        updateUser.setGender("M");
//        userDao.update(updateUser);
//        System.out.println(updateUser);
//        //test delete()
//        User deleteUser = userDao.get(4);
//        userDao.delete(deleteUser);
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        //test getAllOrderBy
        for (User u : userDao.getAll(" user_name like 'u%' ", "user_name asc, user_gender desc")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }
}
