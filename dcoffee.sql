--
-- File generated with SQLiteStudio v3.2.1 on อ. ก.ย. 6 17:01:21 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: catagory
DROP TABLE IF EXISTS catagory;

CREATE TABLE catagory (
    catagory_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                            NOT NULL,
    catagory_name TEXT (50) NOT NULL
);

INSERT INTO catagory (
                         catagory_id,
                         catagory_name
                     )
                     VALUES (
                         1,
                         'cafe'
                     );

INSERT INTO catagory (
                         catagory_id,
                         catagory_name
                     )
                     VALUES (
                         2,
                         'dessert'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                                  NOT NULL,
    product_name        TEXT (50) UNIQUE
                                  NOT NULL,
    product_price       DOUBLE    NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    catagory_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES catagory (catagory_id) ON DELETE RESTRICT
                                                                    ON UPDATE RESTRICT
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        catagory_id
                    )
                    VALUES (
                        1,
                        'Espresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        catagory_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        catagory_id
                    )
                    VALUES (
                        3,
                        'Chocolate Chiffon Cake',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        catagory_id
                    )
                    VALUES (
                        4,
                        'Butter Cake',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
